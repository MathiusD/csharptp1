﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhoAreYouV2
{
    class Program
    {
        static void Main(string[] args)
        {
            int age, current_year=2019, birth_year;
            string name, firstname, age_as_string, sentence_0, sentence_1, sentence_2, sentence_3;
            Console.WriteLine("Saisissez votre nom :");
            name = Console.ReadLine();
            Console.WriteLine("Saisissez votre prenom :");
            firstname = Console.ReadLine();
            do {
                Console.WriteLine("Saisissez votre age :");
                age_as_string = Console.ReadLine();
                int.TryParse(age_as_string, out age);
                if (age < 1)
                {
                    Console.WriteLine("Age Invalide");
                }
            } while (age < 1);
            birth_year = current_year - age;
            sentence_0 = String.Format("Salutation {0} {1}", firstname, name);
            sentence_1 = String.Format(" - Vous êtes âgé de {0} ans,", age);
            sentence_2 = String.Format(" - Vous êtes né(e) en {0}", birth_year);
            if ((birth_year%400==0)||((birth_year%4==0)&&(birth_year!=0))){
                sentence_3 = String.Format(" - Vous êtes né(e) une année bissextille.");
            }
            else
            {
                sentence_3 = String.Format(" - Vous êtes né(e) une année non bissextille.");
            }
            Console.WriteLine(sentence_0);
            Console.WriteLine(sentence_1);
            Console.WriteLine(sentence_2);
            Console.WriteLine(sentence_3);
            Console.ReadKey();
        }
    }
}
