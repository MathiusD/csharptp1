﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhoAreYouV1
{
    class Program
    {
        static void Main(string[] args)
        {
            int age;
            string name, firstname, age_as_string, sentence;
            Console.WriteLine("Saisissez votre nom :");
            name=Console.ReadLine();
            Console.WriteLine("Saisissez votre prenom :");
            firstname=Console.ReadLine();
            Console.WriteLine("Saisissez votre age :");
            age_as_string=Console.ReadLine();
            int.TryParse(age_as_string, out age);
            sentence=String.Format("Salutation {0} {1}, vous avez {2} ans.", firstname, name, age);
            Console.WriteLine(sentence);
            Console.ReadKey();
        }
    }
}
