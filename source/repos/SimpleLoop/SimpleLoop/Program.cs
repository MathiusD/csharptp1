﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLoop
{
    class Program
    {
        static void Main(string[] args)
        {
            string sentence_0="Jusqu'à combien dois-je compter :", output="", space=" ", and="et";
            int request;
            Console.WriteLine(sentence_0);
            int.TryParse(Console.ReadLine(), out request);
            for (int count=1; count<(request+1); count++)
            {
                if (count < request)
                {
                    output = output + count + space;
                }
                else
                {
                    output = output + and + space + count;
                }
            }
            Console.WriteLine(output);
            Console.ReadKey();
        }
    }
}
